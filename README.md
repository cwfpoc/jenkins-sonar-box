Jenkins Sonar Box
=================

vagrant jenkins + sonar box

## Vagrant
Command                              | Description
:--                                    | :--
`vagrant up`                    | start server
`vagrant ssh`                  | access server
`vagrant halt`                  | stop server
`vagrant destroy`           | remove server

## Jenkins
```
http://127.0.0.1:8080
```

## Sonar
```
http://127.0.0.1:9000
```
admin/admin
